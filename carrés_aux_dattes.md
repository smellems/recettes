## Carrés aux dattes

### Crumble
- 1 1/4 tasse cassonade
- 1 1/4 tasse farine
- 2 1/2 tasses gruau
- 1 tasse beurre

### Dattes
- 1/2 tasse eau
- 1/3 tasse cassonnade
- 250 grammes dates

Mélanger cassonade, farine et gruau.  Ajouter beurre et mélanger avec une fourchette.  Garder des chunks.

Faire bouillir eau avec cassonnade et dattes.  Mélanger fréquament.  Laisser refroidir.

Mettre 1/3 du Crumble dans le fond d'un moule et tapper.  Etendre les dattes.  Ajouter reste du Crumble. pas trop tapper.

Cuire à 350 pour 20 minutes.