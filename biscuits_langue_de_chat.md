## Biscuits langue de chat

- 1/2 tasse beurre
- 1/2 tasse sucre
- 2 blancs d'oeufs
- 3/4 tasse farine

Battre beure et sucre. Ajouter blancs d'oeufs et fouetter juste assez. Mélanger farine.

Cuire 8 à 10 minutes sur une plaque à 375 (grille du haut). Bords dorés.