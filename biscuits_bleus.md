## Biscuits bleus

- 1/2 tasse beurre
- 1/2 tasse cassonade
- 1/2 tasse beurre de tournesol
- 1 oeuf
- 1 1/4 tasses avoine
- 1/2 tasse farine de cassava
- 1 c. à thé bicarbonate de soude
- 1 c. à thé vanille
- sel

Battre beurres et sucre. Ajouter oeuf. Mélanger avoine, farine et poudre.

Cuire à 350 pour 11 minutes.