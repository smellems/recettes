## Gâteau pas cuit

- 2 tasses sucre en poudre
- 2 tasses miettes biscuits graham
- 1/2 tasse café fort
- 1 c. à table beure fondu


Mélanger sucre et biscuits.  Ajouter café et beure.

Mélanger à la main et étendre dans un moule.