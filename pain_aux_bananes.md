## Pain aux bananes

- 1/3 tasse beurre
- 2/3 tasse cassonade
- 2 oeufs
- 1 tasse bananes
- 2 c. à table lait sûr
- 2 tasses farine
- 1/2 c. à thé sel
- 1 c. à thé poudre à pâte
- 1/2 c. à thé bicarbonate
- noix, raisin, ..

Mélanger beurre et cassonade. Ajouter oeufs, bananes et lait.  Incorporer les ingrédients secs.

Mettre dans moule à pain graissé.

Cuire à 325 pour 60 minutes.