## Gâteau fridge

- biscuits "Village"
- plein de fraises
- 1 tasse crème fouetter
- 1/4 tasse sucre

Préparer crème fouetté.

Dans un plat, étendre un rang de biscuits "Village", un rang de fraise et couvrir de crème fouetté. Répéter.

Finir avec biscuits et glassage.