## Dominos (Carrés Nanaimo)

### Croute
- 1/2 tasse beurre fondu
- 1/4 tasse sucre
- 1/3 tasse cacao
- 1 oeuf
- 2 tasses chapelure graham
- 1 tasse noix coco
- 1/2 tasse noix haché

### Glassage
- 1/4 tasse beurre
- 2 c. à table poudre pouding instantanné (vannille)
- 3 c. à table lait
- 2 tasses sucre en poudre

### Chocolat
- 3 carrés chocolat mi-sucrés
- 1 c. à table beurre

Mélanger sucre, cacao, chapelure. Ajouter oeuf et beurre. Mélanger et presser dans un moule 9"x9" graissé.

...