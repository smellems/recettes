## Crèpes

- 4 oeufs
- 2 tasses lait
- 1 1/3 tasses farine
- 1 c. à thé poudre à pâte

Cuire avec beurre.


## Crèpes bretonnes

- 2 c. à table beurre
- 1 c. à table sucre
- 3 oeufs
- 2 tasses lait
- 1 tasse farine
- sel

Cuire dans huile.


## Crèpes Seb

- sucre
- beurre
- 2 oeufs
- 2 lait
- 1 tasse farine

Cuire avec gras de bacon.