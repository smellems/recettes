## Pain blanc robot

- 3/4 tasse eau
- 1 1/2 c. à table beurre
- 1 c. à table sucre
- 2 tasses farine
- 2 c. à table lait ou crème
- 1 c. à thé levure instantanée
- Sel

Mettre les ingrédients dans le moule et le moule dans la machine à pain.

Choisir White, Medium crust, Small loaf.  Start!

ou

Pétrir 34 minutes, lever 26 minutes.

Pétrir 15 secondes, lever 25 minutes.

Pétrir 15 secondes, mettre dans moule pour cuisson.

Lever 60 minutes et mettre au four 50 minutes à 350