## Sucre à crème

- 1 lbs beure
- 1 kg cassonade
- 1 kg sucre en poudre
- 370 ml lait évaporé


Mijoter beure, lait, cassonade en mélangeant pour 12 minutes et retirer du feu.

Ajouter sucre en poudre et fouetter au batteur électrique pour 7 minutes.