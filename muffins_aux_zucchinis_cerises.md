## Muffins aux zucchinis et cerises

- 1/3 tasse beurre
- 3/4 tasse sucre
- 3/4 tasse zucchini puré
- 1/2 tasse zucchini haché (avec pépins)
- 1 oeuf
- 1 1/2 tasses farine
- 1 c. à thé poudre à pâte
- 1 c. à thé bicarbonate
- sel
- 40-50 cerises coupées

Réchauffer le four à 475.

Remplir 6 moules à muffins réguliers (extra plein).

Cuire à 375 pour 20 minutes (plus..).