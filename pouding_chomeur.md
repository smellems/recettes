## Pouding chomeur

### Pâte
- 2 c. à table beurre
- 3/4 tasse sucre
- 1 oeuf
- 1/2 tasse lait ou 3/4 tasse banane + 2 c. à table lait
- 1 tasse farine
- 3 c. à thé poudre à pâte

### Sauce
- 1 tasse eau
- 1 tasse cassonade
- 2 c. à table beurre

Battre beurre et sucre. Ajouter oeuf et lait.  Mélanger farine et poudre.

Faire bouillir eau, cassonade et beurre. Laisser refroidir.

Étendre Pâte dans un moule graissé.  Verser lentement Sauce sur le dessus.

Cuire à 350 pour 30 minutes.